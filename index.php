<?php

ini_set('xdebug.var_display_max_data', 1000);
ini_set('xdebug.var_display_max_children', 128);
ini_set('xdebug.var_display_max_depth', 10);

function normalize_phpinfo(&$input)
{
    // remove  carriage return (\r) + line feed (\n) with just a line feed (\n) character
    $input = str_replace("\r\n", "\n", $input);

    // there shouldn't be more than 2 line feed (\n) characters
    $input = preg_replace('/(\n{3,})/', "\n\n", $input);

    // if the phpinfo data is from cli rather than phpinfo()
    if (substr_count($input, 'PHP Version => ', 0, 100) !== 0) {

        // remove phpinfo() from the begining if that exists
        if (substr($input, 0, 10) === 'phpinfo()' . "\n") {
            $input = substr($input, 10);
        }

        // replace all '=>' with '\t'
        $input = str_replace('=>', "\t", $input);
    }
    else {
        // the php version string on top should have a tab separator, not space
        $input = str_replace('PHP Version ', 'PHP Version' . "\t", $input);
    }

    // patch comma separated list of new lines
    $input = str_replace(',' . "\n", ', ', $input);
}

function phpinfo_to_array($input)
{
    // resulting array();
    $output = array();

    // title
    $title = null;

    // main level
    foreach (explode("\n\n", $input) as $i => $group) {
    	// default title
    	if ($title === null && $i > 0) {
    		$title = 'System';
		}
    		
        // skip some basics
        if ($group === 'PHP Credits' || 
            $group === 'Configuration' ||
            substr_count($group, 'This server is protected with the Suhosin Extension') ||
            substr_count($group, 'mbstring extension makes use of')) {
            continue;
        }

        // if the group doesn't have any new lines or tabs, then it must be a new section
        if (substr_count($group, "\n") === 0 && substr_count($group, "\t") === 0) {
            $title = trim($group);
            continue;
        }

        // if there are multiple lines, go deep
        if (substr_count($group, "\n") !== 0) {
            // separate out different lines
            $group = explode("\n", $group);
        }

        phpinfo_to_array_add($output, $title, $group);
    }
    
    return $output;
}

function phpinfo_to_array_add(&$array, $key, $data)
{
    if (is_array($data)) {
        $t_array = array();
        foreach ($data as $lines) {
            phpinfo_to_array_add_data($t_array, $lines);
        }

        if (!empty($t_array)) {
            if ($key !== null) {
                if (isset($array[$key])) {
                    $array[$key] = array_merge($array[$key], $t_array);
                }
                else {
                    $array[$key] = $t_array;
                }
            }
            else {
                $array[] = $t_array;
            }
        }
    }
    else {
        if ($key !== null) {
            phpinfo_to_array_add_data($array[$key], $data);
        }
        else {
            phpinfo_to_array_add_data($array, $data);
        }
    }
}

function phpinfo_to_array_add_data(&$array, $data)
{
    // skip the 'Directive  Local Value  Master Value' line
    if (strlen($data) >= 9 && substr_count($data, 'Directive', 0, 9) !== 0) {
        return;
    }

    // only add if there is a tab separator
    if (substr_count($data, "\t") !== 0) {
        $line = explode("\t", $data);
    
		// remove extra data from php version number
		if ($line[0] === 'PHP Version' && substr_count($line[1], '-')) {
			$version = explode('-', $line[1]);
			$line[1] = $version[0];
		}
    
        if (count($line) === 3) {
            $array[trim($line[0])] = array(
                'local' => trim($line[1]),
                'master' => trim($line[2])
            );
        }
        else {
            $array[trim($line[0])] = trim($line[1]);
        }
    }
}

function phpinfo_diff($left, $right)
{
    // ignore these
    $ignores = array(
        'Apache Environment',
        'HTTP Headers Information',
        'PHP License'
    );

    $diff = array();

    foreach ($left as $key => $val) {
        phpinfo_diff_insert($diff[$key], $val, 'left');
    }
    foreach ($right as $key => $val) {
        phpinfo_diff_insert($diff[$key], $val, 'right');
    }

    foreach ($diff as $key => $val) {
        phpinfo_diff_compare($diff[$key], $key);
    }

    foreach ($ignores as $ignore) {
        if (isset($diff[$ignore])) {
            unset($diff[$ignore]);
        }
    }

    uksort($diff, 'strnatcasecmp');
    $php_version = $diff['PHP Version'];
    $system = $diff['System'];
    unset($diff['PHP Version']);
    unset($diff['System']);
    $diff = array_merge(array('PHP Version' => $php_version, 'System' => $system), $diff);
    
    return $diff;
}

function phpinfo_diff_insert(&$array, $value, $direction)
{   
    if (is_array($value)) {
        foreach ($value as $key => $val) {
            phpinfo_diff_insert($array[$key], $val, $direction);
        }
    }
    else {
        $array[$direction] = $value;
    }
}

function phpinfo_diff_compare(&$array, $parent_key)
{
    // non major comparisons
    $minor_settings = array(
        'System',
        'Build Date',
        'Configure Command',
        'Configuration File (php.ini) Path',
        'Loaded Configuration File',
        'Scan this dir for additional .ini files',
        'Additional .ini files parsed',
        
    );

    // compare levels
    $levels = array(
        0 => '88C100', // green
        1 => 'FABE28', // yellowish
        2 => 'FF8A00', // orange
        3 => 'FF003C'  // red
    );

    // local/master value
    if (isset($array['local']) && isset($array['master'])) {
        phpinfo_diff_compare($array['local'], 'local');
        phpinfo_diff_compare($array['master'], 'master');
    }
    // has left/right value
    else if (isset($array['left']) || isset($array['right'])) {
        // almost (ignore) these
        if (in_array($parent_key, $minor_settings)) {
            if (isset($array['left']) && isset($array['right']) &&
                $array['left'] === $array['right']) {
                $array['status'] = 'match';
                $array['color']  = $levels[0];
            }
            else {
                $array['status'] = 'minor-mismatch';
                $array['color']  = $levels[1];
            }
        }
        // missing from either
        else if (!isset($array['left']) || !isset($array['right'])) {
            $missing_side = !isset($array['left']) ? 'left' : 'right';
            $array['status'] = 'missing: ' . $missing_side;
            $array['color']  = $levels[3];
        }
        // both exist and match perfectly
        else if ($array['left'] === $array['right']) {
            $array['status'] = 'match';
            $array['color']  = $levels[0];
        }
        else {
            // high alert ones
            if (substr_count(strtolower($parent_key), 'version')) {
                $array['status'] = 'version-mismatch';
                $array['color']  = $levels[3];
            }
            // normal
            else {
                $array['status'] = 'no-mismatch';
                $array['color']  = $levels[2];
            }
        }
    }
    // array
    else {
        foreach ($array as $key => $val) {
            phpinfo_diff_compare($array[$key], $key);
        }
    }
}

// form submission
if (isset($_POST['left']) && isset($_POST['right']))
{
    $left  = trim($_POST['left']);
    $right = trim($_POST['right']);

    // normalize `php -i` and phpinfo()'s output to a standard
    normalize_phpinfo($left);
    normalize_phpinfo($right);

    // turn into array
    $leftData = phpinfo_to_array($left);
    $rightData = phpinfo_to_array($right);

    // do the diff
    $diff = phpinfo_diff($leftData, $rightData);
}

function phpinfo_print_diff($diff)
{
    $local_master = false;

    foreach ($diff as $key => $val) {
        if (!isset($val['left']) && !isset($val['right'])) {
            // print separator
            echo '<tr><td class="break" colspan="5"></td></tr>';

            // print header
            echo '<tr><td class="title" colspan="5">';
            echo '<h2><a name="' . strtolower($key) . '">' . $key . '</a></h2>';
            echo '</td></tr>';

            // print separator
            echo '<tr><td class="break" colspan="5"></td></tr>';

            // print heading
            echo '<tr>';
            echo '<td class="h">Directive</td>';
            echo '<td class="h" colspan="2">Left phpinfo()</td>';
            echo '<td class="h" colspan="2">Right phpinfo()</td>';
            echo '</tr>';

            // print rows
            foreach ($val as $k => $v) {
                phpinfo_print_diff_row($k, $v, $local_master);
            }
        }
        else {
            // print separator
            echo '<tr><td class="break" colspan="5"></td></tr>';

            // print row
            phpinfo_print_diff_row($key, $val, $local_master);
        }
    }
}

function phpinfo_print_diff_row($key, $val, &$local_master)
{
    // if left AND right isn't set, there is a master/local value pair
    if (!isset($val['left']) && !isset($val['right'])) {
        if ($local_master !== true) {
            // print separator
            echo '<tr><td class="break" colspan="5"></td></tr>';

            // print heading
            echo '<tr>';
            echo '<td class="h">Directive</td>';
            echo '<td class="h" colspan="2">Left phpinfo()</td>';
            echo '<td class="h" colspan="2">Right phpinfo()</td>';
            echo '</tr>';

            // print sub-heading
            echo '<tr>';
            echo '<td class="h"></td>';
            echo '<td class="h">Local Value</td>';
            echo '<td class="h">Master Value</td>';
            echo '<td class="h">Local Value</td>';
            echo '<td class="h">Master Value</td>';
            echo '</tr>';
        }
        echo '<tr>';
        echo '<td class="e">' . $key . '</td>';
        echo '<td class="v" style="background-color: #' . $val['local']['color'] . ';">' . (isset($val['local']['left']) ? $val['local']['left'] : '') . '</td>';
        echo '<td class="v" style="background-color: #' . $val['master']['color'] . ';">' . (isset($val['master']['left']) ? $val['master']['left'] : '') . '</td>';
        echo '<td class="v" style="background-color: #' . $val['local']['color'] . ';">' . (isset($val['local']['right']) ? $val['local']['right'] : '') . '</td>';
        echo '<td class="v" style="background-color: #' . $val['master']['color'] . ';">' . (isset($val['master']['right']) ? $val['master']['right'] : '') . '</td>';
        echo '</tr>';

        $local_master = true;
    }
    else {
        $local_master = false;

        echo '<tr>';
        echo '<td class="e">' . $key . '</td>';
        echo '<td class="v" colspan="2" style="background-color: #' . $val['color'] . ';">' . (isset($val['left']) ? $val['left'] : '') . '</td>';
        echo '<td class="v" colspan="2" style="background-color: #' . $val['color'] . ';">' . (isset($val['right']) ? $val['right'] : '') . '</td>';
        echo '</tr>';
    }
}

?><!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body {background-color: #ffffff; color: #000000;}
        body, td, th, h1, h2 {font-family: sans-serif;}
        pre {margin: 0px; font-family: monospace;}
        a:link {color: #000099; text-decoration: none; background-color: #ffffff;}
        a:hover {text-decoration: underline;}
        table {border-collapse: collapse; margin-bottom: 100px;}
        .center {text-align: center;}
        .center table { margin-left: auto; margin-right: auto; text-align: left;}
        .center th { text-align: center !important; }
        td, th { border: 1px solid #000000; font-size: 75%; vertical-align: baseline;}
        h1 {font-size: 150%;}
        h2 {font-size: 125%;}
        .p {text-align: left;}
        .e {background-color: #ccccff; font-weight: bold; color: #000000;}
        .h {background-color: #9999cc; font-weight: bold; color: #000000;}
        .v {background-color: #cccccc; color: #000000;}
        .vr {background-color: #cccccc; text-align: right; color: #000000;}
        .h {background-color: #ccccff; font-weight: bold; color: #000000; text-align: center;}
        img {float: right; border: 0px;}
        hr {width: 600px; background-color: #cccccc; border: 0px; height: 1px; color: #000000;}
        form {margin: 0 auto; text-align: center; width: 1000px; height: 800px; margin-bottom: 60px;}
        textarea {width: 475px; height: 800px;}
        td.break {height: 20px; border: 0;}
        td.title {text-align: center; border: 0;}
    </style>
    <title>phpinfo()</title>
    <meta name="ROBOTS" content="NOINDEX,FOLLOW,NOARCHIVE">
</head>
<body>
    <?php if (isset($diff)): ?>
    <div class="center">
        <table border="0" cellpadding="3" width="600">
            <tr class="h">
                <td class="title" colspan="5">
                    <h1 class="center">PHP Diff</h1>
                </td>
            </tr>
            <tr><td class="break" colspan="5"></td></tr>
            <tr>
                <td class="title" colspan="5">
                    <h1>PHP</h1>
                </td>
            </tr>
            <?php echo phpinfo_print_diff($diff); ?>
        </table>
    </div>
    <?php endif; ?>
    <form method="POST">
        <textarea name="left" placeholder="Paste phpinfo()/php -i output from server-1"><?php echo isset($_POST['left']) ? $_POST['left'] : ''; ?></textarea>
        <textarea name="right" placeholder="Paste phpinfo()/php -i output from server-2"><?php echo isset($_POST['right']) ? $_POST['right'] : ''; ?></textarea>
        <br>
        <input type="submit" value="Diff!">
    </form>
</body>
</html>